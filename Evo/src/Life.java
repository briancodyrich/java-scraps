import java.util.ArrayList;
import java.util.Random;


public class Life implements Comparable<Life>
{
	public final int MAX_GENES = 4;
	public String bodyGenes;
	public String behaviorGenes;
	public int health;
	public int max_health;
	public int food;
	public int max_food;
	public int maxhp = 5;
	public int hp = maxhp;
	public int maxmp = 1;
	public int maxip = 0;
	public int ip = maxip;
	public int mp = maxmp;
	public int maxfp = 100;
	public int fp = maxfp;
	public int upkeep = 1;
	public int sp = 0;
	public int str = 0;
	public int def = 0;
	public int spd = 0;
	public int mnd = 0;
	public int id = 0;
	public static int idNUM = 0;
	public int breedpoints = 0;
	public int offspring_size = 0;
	public int longevity = 10;
	public int pp = 0;
	public int brain = 0;
	public int pivot = 0;
	public int strP = 0;
	public int intP = 0;
	public int plantP = 0;
	public int age = 0;
	public int evoP = 0;
	private Random rand;
	public Life(GeneManager man)
	{
		id = ++idNUM;
		rand = new Random();
		bodyGenes = "" + randomGene(MAX_GENES) + randomGene(MAX_GENES) + randomGene(MAX_GENES) + randomGene(MAX_GENES) + randomGene(MAX_GENES) + randomGene(MAX_GENES) + randomGene(MAX_GENES) + randomGene(MAX_GENES) + randomGene(MAX_GENES);
		//pivot = genes.length() +1;
		behaviorGenes = "" + randomGene(MAX_GENES) + randomGene(MAX_GENES) + randomGene(MAX_GENES) + randomGene(MAX_GENES) + randomGene(MAX_GENES) + randomGene(MAX_GENES) + randomGene(MAX_GENES) + randomGene(MAX_GENES) + randomGene(MAX_GENES);
		//genes += "X" + randomGene(MAX_GENES) + randomGene(MAX_GENES) + randomGene(MAX_GENES) + randomGene(MAX_GENES);
		man.GetStats(this);

	}
	public Life(Life life, Life life2, int rate, GeneManager man)
	{
		id = ++idNUM;
	//	System.out.println("hey");
		rand = new Random();
		bodyGenes = "";
		behaviorGenes = "";
		for (int x = 0; x < Math.max(life.bodyGenes.length(), life2.bodyGenes.length()) / 2; x++)
		{
			boolean mutate = false;
			if (rand.nextInt(101) < rate) mutate = true;
			if (rand.nextInt(101) > 50)
			{
				if (x*2 < life.bodyGenes.length() -1)
				{
					if (mutate) {
						int r = rand.nextInt(101);
						if (r < 15) {
							bodyGenes += life.bodyGenes.substring(x*2, x*2+2) + life.bodyGenes.substring(x*2, x*2+2);
						} else if (r < 30) {
							bodyGenes += life.bodyGenes.substring(x*2, x*2+1) + life.bodyGenes.substring(x*2 + 1, x*2+1);
						} else if (r < 50) {
							bodyGenes += life.bodyGenes.substring(x*2, x*2+1) + life.bodyGenes.substring(x*2 + 1, x*2+1) + life.bodyGenes.substring(x*2 + 1, x*2+1) + life.bodyGenes.substring(x*2, x*2+1);
						} else if (r < 60) {
							bodyGenes += life.bodyGenes.substring(x*2, x*2+1) + life.bodyGenes.substring(x*2 + 1, x*2+1) + life.bodyGenes.substring(x*2, x*2+1) + life.bodyGenes.substring(x*2 + 1, x*2+1);
						} else {
							//bodyGenes += life.bodyGenes.substring(x*2, x*2+1);
						}
					} else {
						bodyGenes += life.bodyGenes.substring(x*2, x*2+2);
					}
					
				} else {
					bodyGenes += "" + randomGene(MAX_GENES) + randomGene(MAX_GENES);
				}
			} else {
				if (x*2 < life2.bodyGenes.length() - 1)
				{
					if (mutate) {
						int r = rand.nextInt(101);
						if (r <15) {
							bodyGenes += life2.bodyGenes.substring(x*2, x*2+2) + life2.bodyGenes.substring(x*2, x*2+2);
						} else if (r < 30) {
							bodyGenes += life2.bodyGenes.substring(x*2, x*2+1) + life2.bodyGenes.substring(x*2 + 1, x*2+1);
						} else if (r < 50) {
							bodyGenes += life2.bodyGenes.substring(x*2, x*2+1) + life2.bodyGenes.substring(x*2 + 1, x*2+1) + life2.bodyGenes.substring(x*2 + 1, x*2+1) + life2.bodyGenes.substring(x*2, x*2+1);
						} else if (r < 60) {
							bodyGenes += life2.bodyGenes.substring(x*2, x*2+1) + life2.bodyGenes.substring(x*2 + 1, x*2+1) + life2.bodyGenes.substring(x*2, x*2+1) + life2.bodyGenes.substring(x*2 + 1, x*2+1);
						} else {
							//bodyGenes += life2.bodyGenes.substring(x*2, x*2+1);
						}
					} else {
						bodyGenes += life2.bodyGenes.substring(x*2, x*2+2);
					}
					
				} else {
					bodyGenes += "" + randomGene(MAX_GENES) + randomGene(MAX_GENES);
				}				
			}
		}
	//	System.out.println("XXX");
	//	System.out.println(bodyGenes);
		
		for (int x = 0; x < Math.max(life.behaviorGenes.length(), life2.behaviorGenes.length()) / 2; x++)
		{
			boolean mutate = false;
			if (rand.nextInt(101) < rate) mutate = true;
			if (rand.nextInt(101) > 50)
			{
				if (x*2 < life.behaviorGenes.length() - 1)
				{
					if (mutate) {
						int r = rand.nextInt(101);
						if (r < 15) {
							behaviorGenes += life.behaviorGenes.substring(x*2, x*2+2) + life.behaviorGenes.substring(x*2, x*2+2);
						} else if (r < 30) {
							behaviorGenes += life.behaviorGenes.substring(x*2, x*2+1) + life.behaviorGenes.substring(x*2 + 1, x*2+1);
						} else if (r < 50) {
							behaviorGenes += life.behaviorGenes.substring(x*2, x*2+1) + life.behaviorGenes.substring(x*2 + 1, x*2+1) + life.behaviorGenes.substring(x*2 + 1, x*2+1) + life.behaviorGenes.substring(x*2, x*2+1);
						} else if (r < 60) {
							behaviorGenes += life.behaviorGenes.substring(x*2, x*2+1) + life.behaviorGenes.substring(x*2 + 1, x*2+1) + life.behaviorGenes.substring(x*2, x*2+1) + life.behaviorGenes.substring(x*2 + 1, x*2+1);
						} else {
							//behaviorGenes += life.behaviorGenes.substring(x*2, x*2+1);
						}
					} else {
						behaviorGenes += life.behaviorGenes.substring(x*2, x*2+2);
					}
					
				} else {
					behaviorGenes += "" + randomGene(MAX_GENES) + randomGene(MAX_GENES);
				}
			} else {
				if (x*2 < life2.behaviorGenes.length() - 1)
				{
					if (mutate) {
						int r = rand.nextInt(101);
						if (r < 15) {
							behaviorGenes += life2.behaviorGenes.substring(x*2, x*2+2) + life2.behaviorGenes.substring(x*2, x*2+2);
						} else if (r < 30) {
							behaviorGenes += life2.behaviorGenes.substring(x*2, x*2+1) + life2.behaviorGenes.substring(x*2 + 1, x*2+1);
						} else if (r < 50) {
							behaviorGenes += life2.behaviorGenes.substring(x*2, x*2+1) + life2.behaviorGenes.substring(x*2 + 1, x*2+1) + life2.behaviorGenes.substring(x*2 + 1, x*2+1) + life2.behaviorGenes.substring(x*2, x*2+1);
						} else if (r < 60) {
							behaviorGenes += life2.behaviorGenes.substring(x*2, x*2+1) + life2.behaviorGenes.substring(x*2 + 1, x*2+1) + life2.behaviorGenes.substring(x*2, x*2+1) + life2.behaviorGenes.substring(x*2 + 1, x*2+1);
						} else {
							//behaviorGenes += life2.behaviorGenes.substring(x*2, x*2+1);
						}
					} else {
						behaviorGenes += life2.behaviorGenes.substring(x*2, x*2+2);
					}
					
				} else {
					behaviorGenes += "" + randomGene(MAX_GENES) + randomGene(MAX_GENES);
				}				
			}
		}		
		
	//	System.out.println(behaviorGenes);
		
		
		man.GetStats(this);
		
		/*
		for (int x = 0; x < Math.max(life.length(), life2.length()); x++)
		{
			boolean mutate = false;
			
			if (rand.nextInt(101) < rate) mutate = true;
			if (rand.nextInt(101) > 50)
			{
				if (x < life.length())
				{
					if (mutate) {
						int value = rand.nextInt(100);
						if (value < 40) {
							genes += "" + life.charAt(x)+ life.charAt(x);
						} else if (value < 80) {
							genes += "" +  randomGene(MAX_GENES);
						}
					} else {
						genes += "" + life.charAt(x);
					}
				} else {
					genes += "" + randomGene(MAX_GENES);
				}
			} else {
				if (x < life2.length())
				{
					if (mutate) {
						int value = rand.nextInt(100);
						if (value < 30) {
							genes += "" + life2.charAt(x) + randomGene(MAX_GENES);
						} else if (value < 60) {
							genes += "" + randomGene(MAX_GENES);
						}
					} else {
						genes += "" + life2.charAt(x);
					}
					
				} else {
					genes += "" + randomGene(MAX_GENES);
				}
			}
		}
		while (pivot == 0)
		{
			//System.out.println(genes);
			//System.out.println(genes.length());
			int num = rand.nextInt(genes.length()-1);
			///System.out.println(num);
			//System.out.println(genes.substring(0, num));
			//System.out.println(genes.substring(num + 1, genes.length()-1));
			genes = genes.substring(0, num) + "X" + genes.substring(num + 1, genes.length()-1);
		}
		*/
	}
	
	public void GetUpkeep()
	{
		upkeep = 1 + bodyGenes.length() / 2;
	}

	
	
	public void tic(double ratio, GeneManager gp, ArrayList<Life> lifeL)
	{
		mp++;
		mp = Math.min(mp, maxmp);
		int counter = age++;
		for (int x = 0; x <= maxmp && mp > 0; x++)
		{
			gp.GetBehavior(this, counter, ratio, lifeL);
			counter++;
		}
		
		if ((float)fp / maxfp > .75 && (str + spd + def) > 0)
		{
			breedpoints++;
		} else {
			breedpoints--;
		}
		fp -= (upkeep + (age / longevity));
		while (fp < 0)
		{
			hp--;
			fp++;
		}
	}
		
	public void Forage(double ratio)
	{
		//System.out.println(ratio*100 + "  " + strP);
		//System.out.println("forage");
		if (fp >= maxfp && ip > 0)
		{
			ip--;
			return;
		}
		if (mp <= 0) return;
		mp--;
		ratio *= (1 + (float)mnd / 20.0);
		if (rand.nextInt(100) > (ratio*100)) return;
		fp += 10;
		fp += (str * 3);
		fp += (mnd * 3);
		fp = Math.min(fp, maxfp);
	}
	public void Fight(ArrayList<Life> lifeL)
	{
		if (lifeL.size() < 2) return;
		if (fp >= maxfp && ip > 0)
		{
			ip--;
			return;
		}
		if (mp <= 0) return;
		mp--;
		//System.out.println("fight");
		for(;;)
		{
			Life L = lifeL.get(rand.nextInt(lifeL.size()));
			if (L.id != id)
			{
				if (L.sp >= 0)
				{
					L.sp--;
					return;
				}
				for (int x = 0; x < 100; x++)
				{
					L.hp -= Math.max(0, str - L.def);
					if (L.hp > 0)
					{
						hp -= Math.max(0, L.str - def);
					}
					if (L.hp <= 0)
					{
						fp += L.fp + (L.maxhp * 2);
						fp = Math.min(fp, maxfp);
						return;
					}
					if (hp <= 0)
					{
						L.fp += fp + (maxhp * 2);
						L.fp = Math.min(L.fp, L.maxfp);
						return;
					}
				}
				return;
			}
		}
	}
	
	public void Rest()
	{
		hp += 1;
		mp += 2;
		ip = maxip;
		mp = Math.min(mp, maxmp);
		hp = Math.min(hp, maxhp);
	}
	public void Heal()
	{
		hp += 5;
		hp = Math.min(hp, maxhp);	
	}
	
	public void Breed()
	{

	}
	
	private char randomGene(int max)
	{
		return ((char)(rand.nextInt(max - 1) + 65));
	}
	
	public boolean isDead()
	{
		if (hp <= 0) return true;
		if (behaviorGenes.length() == 0) return true;
		if (bodyGenes.length() == 0) return true;
		return false;
	}
	
	public String PrintStats(GeneManager man)
	{
		String rstr = "";
		rstr += "(hp:" + hp + "/" + maxhp + ") " + "(mp:" + mp + "/" + maxmp + ")" + " (fp:" + fp + "/" + maxfp +  "(-" + (upkeep + (age / longevity))  +"))" + breedpoints + "\n";
		rstr += "str: " + str + " def:" + def + " spd:" + spd + " mnd:" + mnd + "\n";
		rstr += man.PrintGenes(this) + "\n";
		rstr += man.PrintBehavior(this) + "\n";
		return rstr;
		//return "h: " + health + "/" + max_health + " f:" + food + "/" + max_food + al "-" + (upkeep + (age / 10)) + "\n" + PrintDna() + "\n" + "str: " + str +  " int: " + brain + "  " + pivot;
	}
	
	public int compareTo(Life arg0) {
		if (arg0.breedpoints == breedpoints) return 0;
		if (arg0.breedpoints > breedpoints)
		{
			return 1;
		} else 
			return -1;
	}
}
