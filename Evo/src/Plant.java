import java.util.ArrayList;
import java.util.Random;


public class Plant implements Comparable<Plant>
{
	public final double AGE_COST = .00004;
	
	public double storage = 2000;
	public double age = 1;
	public double bp = 0;
	public String genes = "";
	private Random rand;
	
	public Plant()
	{
		rand = new Random();
		age = 1;
		genes = "" + randomGene() + randomGene() + randomGene() + randomGene() + randomGene() + randomGene() + randomGene() + randomGene() + randomGene() ;
		//System.out.println(genes);
		//genes = "ATK";
	}
	public Plant(String a, String b)
	{
		rand = new Random();
		age = 1;
		char next;
		for (int x = 0; x < a.length() || x < b.length(); x++)
		{
			if (x < a.length() && x < b.length())
			{
				if (rand.nextInt(100) > 50)
				{
					next = a.charAt(x);
				} else {
					next = b.charAt(x);
				}
				
				if (rand.nextInt(100) > 96)
				{
					int mutate = rand.nextInt(100);
					if (mutate < 35)
					{
						genes += next;
						genes += next;
					} else if (mutate <70) {
						genes += Character.toString(randomGene());
					}
				} else {
					genes += next;
				}
			} else if (x < a.length()){
				genes += a.charAt(x);
			} else {
				genes += b.charAt(x);
			}
		}
		for (int x = 0; x < genes.length(); x++)
		{
			if (Character.isAlphabetic(genes.charAt(x)) == false) storage = -1000;
		}
		//System.out.println(genes + "/" + a + "+" + b);
	}
	public double getValue()
	{
		if (bp < 2000 || storage <= 1500) return 0;
		return bp;
	}
	public boolean isDead()
	{
		if (storage <= 0)
		{
			return true;
		}
		if (age <= 0.5) return true;
		return false;
	}
	
	public int compareTo(Plant arg0) {
		if (arg0.getValue() == getValue()) return 0;
		if (arg0.getValue() > getValue())
		{
			return 1;
		} else 
			return -1;
	}
	
	public void tic(int hour, int light, int pop)
	{
		age();
		light *= Math.min(10000.0 / pop, 1.2);
		double cost = age;
		for (int x = 0; x < genes.length() / 3; x++)
		{
			
			int type = geneToInt(genes.charAt(x * 3));
			if (isNow(hour , genes.charAt((x * 3) + 1) , genes.charAt((x * 3) + 2)))
			{
				if (type < 0)
				{
					cost += 1000;
				} else if (type <= 4 && type >= 0) //sun
				{
					age();
					//System.out.println(light);
					cost += 1;
					cost -= light / 300.0;
					light *= .8;
					if (light >= 0 && storage >= 1500)
					{
						cost += .2;
						bp += 1;
					} else {
						bp -= .5;
					}
				} else if (type <= 9) { //breed
					age();
					if (light >= 0 && storage >= 1500)
					{
						cost += 2;
						bp += 2;
					} else {
						bp -= 1.5;
					}
				} else if (type <= 14) { //hybernate
					cost += 0.2;
					if (light == 0) cost = 0;
					
				} else if (type <= 19) { //efficiency
					cost *= .99;
				} else if (type <= 24) { //longevity
					deage();
				}  else {
					cost += 1000;
				}
			}
			
		}
		storage -= cost;
		storage -= age;
	}
	
	private void age()
	{
		age += AGE_COST;
	}
	private void deage()
	{
		age -= AGE_COST / 3;
	}
	public int geneToInt(char c)
	{
		return c - 65;
	}
	private char randomGene()
	{
		return ((char)(rand.nextInt(26) + 65));
	}
	public boolean isNow(int h, char a, char b)
	{
		int start = (geneToInt(a));
		int end = (geneToInt(b));
		//System.out.println(h + "/" + start + "/" + end);
		if (h >= start && h <=  start + end) return true;
		if (start + end > 24)
		{
			if (h <= start && h <= ((start + end) % 24)) return true;
		}
		return false;
	}

}
