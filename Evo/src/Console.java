import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Scanner;


public class Console 
{
	String str;
	int break_cap = Integer.MAX_VALUE;
	//BufferedReader br ;
	public EvoManager manager;
	
	public Console (EvoManager manager)
	{
		this.manager = manager;
	}
	

	public void run() throws IOException 
	{
		Scanner scan = new Scanner(System.in);
		for (;;)
		{
			
			System.out.println(">");
			
			while (!scan.hasNextLine());
			Scanner parser = new Scanner(scan.nextLine());
			switch (parser.next())
			{
				case "exit":
					System.out.println("EXIT");
					System.exit(0);
					break;
				case "seed":
					if (parser.hasNextInt()) manager.Seed(parser.nextInt());
					break;
				case "print":
					manager.Print(parser);
					break;
				case "dump":
					manager.Dump(parser);
					break;
				case "clear":
					manager.lifeL.clear();
					manager.genepool.Reorder();
					break;
				case "food":
					if (parser.hasNextInt()) manager.Food(parser.nextInt());
					break;
				case "break":
					if (parser.hasNextInt()) break_cap = parser.nextInt();
					break;
				case "mutate":
					if (parser.hasNextInt()) manager.Mutate(parser.nextInt());
					break;
				case "run":
					if (parser.hasNextInt()) 
					{
						int limit = parser.nextInt();
						int percent = 0;
					//	if (limit < 2000) percent = 101;
						System.out.println("[####################]");
						System.out.print("[");
						for(int x = limit; x > 0; x--) 
						{
							if (percent < 100)
							{
								int locP = (limit - x) * 100 / limit;
								while(locP > percent)
								{
									if (percent % 5 == 0)
										System.out.print("|");
									percent++;
									
								}
							}
							manager.run();
							if (manager.lifeL.size() >= break_cap || manager.lifeL.size() == 0) break;
						}
					}
					else {
						for (;;) manager.run();
					}
					System.out.println("]\ndone");
			}
			parser.close();
		}
	}
}
