import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Scanner;


public class EvoManager
{
	ArrayList<Life> lifeL = new ArrayList<Life>();
	GeneManager genepool;
	int counter;
	int food;
	int rate;
	public EvoManager()
	{
		food = 0;
		rate = 0;
		genepool = new GeneManager();
	}
	public void Seed(int seed)
	{
		for (int x = 1; x <= seed; x++) lifeL.add(new Life(genepool));
	}
	public void Food(int food)
	{
		this.food = food;
	}
	public void Mutate(int rate)
	{
		this.rate = rate;
	}
	public void Print(Scanner scan)
	{
		
		if (scan.hasNext())
		{
			String str = scan.next();
			int min = 0;
			int max = 0;
			if (scan.hasNextInt()) min = scan.nextInt();
			if (scan.hasNextInt()) max = scan.nextInt();
			if (min > max)
			{
				max = min;
				min = 0;		
			}
			if (max == 0) max = lifeL.size();
			switch (str)
			{
				case "pop":
					
					System.out.println("pop: " + lifeL.size());
					break;
				case "stats":
					for (int x = min; x < max && x < lifeL.size(); x++)
					{
						System.out.println("[" + (x+1) + "] " + lifeL.get(x).PrintStats(genepool));
					}
					break;
			}
			
		}
	}
	
	public void Dump(Scanner scan) throws IOException
	{
		if (scan.hasNext())
		{
			String str = scan.next();
			boolean append = false;
			if (scan.hasNextBoolean()) append = scan.nextBoolean();
			PrintWriter writer = new PrintWriter(new FileWriter(str, append));
			for (int x = 0; x < lifeL.size(); x++)
			{
				writer.println(lifeL.get(x).PrintStats(genepool));
			}
			writer.close();
		}
		
	}
	public void run()
	{
		counter++;
		for(int x = 0; x < lifeL.size(); x++)
		{
			Life life = lifeL.get(x);
			if (life.isDead())
			{
				lifeL.remove(x);
				life = null;
			} else {
				life.tic((double) food / lifeL.size(), genepool, lifeL);
			}
		}
		if (counter % 10 == 0)
		{
			Collections.sort(lifeL);
			for(int x = 0; x < lifeL.size() - 1; x++)
			{
				if (lifeL.get(x).breedpoints > 75 && lifeL.get(x+1).breedpoints > 75)
				{
					for (int y = 0; y < 2 + lifeL.get(x).offspring_size + lifeL.get(x+1).offspring_size; y++)
						lifeL.add(new Life(lifeL.get(x), lifeL.get(x+1), rate, genepool));
					lifeL.get(x).breedpoints -= 75;
					lifeL.get(x+1).breedpoints -= 75;
					x+=2;
				}
			}
		}
	}
}
