import java.util.ArrayList;
import java.util.Random;


public class GeneManager 
{
	private String[] bodyGenes;
	private String[] behaviorGenes;
	
	public GeneManager()
	{
		Reorder();
	}
	
	public String PrintBehavior(Life life)
	{
		String str = "";
		for (int x = 0; x < life.behaviorGenes.length() / 2; x++)
		{
			switch (GenesAt(life.behaviorGenes.substring(x, x+2)))
			{
				case 0: //strs
					str += ("(ZZZ)");
					break;
				case 1: //def
					str += ("(NOM)");
					break;
				case 2: //spd
					str += ("(KIL)");
					break;
				case 3: //int
					str += ("(HEL)");
					break;
				case 4: //offspring size
					str += ("(BRD)");
					break;
				case 5: //body size
					str += ("(NOM)");
					break;
				case 6: //stomach size
					str += ("(NOM)");
					break;
				case 7: //longevity
					str += ("(NUL)");
					break;
				case 8: //stealth
					str += ("(NUL)");
					break;
				default:
					System.out.println("gene error");
					
			}
		}
		return str;
	}
	
	public String PrintGenes(Life life)
	{
		String str = "";
		for (int x = 0; x < life.bodyGenes.length() / 2; x++)
		{
			switch (GenesAt(life.bodyGenes.substring(x, x+2)))
			{
				case 0: //str
					str += ("(STR)");
					break;
				case 1: //def
					str += ("(DEF)");
					break;
				case 2: //spd
					str += ("(SPD)");
					break;
				case 3: //int
					str += ("(INT)");
					break;
				case 4: //offspring size
					str += ("(KID)");
					break;
				case 5: //body size
					str += ("(BOD)");
					break;
				case 6: //stomach size
					str += ("(FOD)");
					break;
				case 7: //longevity
					str += ("(LOG)");
					break;
				case 8: //stealth
					str += ("(STL)");
					break;
				default:
					System.out.println("gene error");
					
			}
		}
		return str;
	}
	
	public void GetBehavior(Life life, int counter, double ratio, ArrayList<Life> lList)
	{
		int x = counter % (life.behaviorGenes.length() / 2);
		switch (GenesAt(life.behaviorGenes.substring(x, x+2)))
		{
			case 0: //rest
				life.Rest();
				break;
			case 1: //forage
				life.Forage(ratio);
				break;
			case 2: //fight
				life.Fight(lList);
				break;
			case 3: //heal
				life.Heal();
				break;
			case 4: //breed
				break;
			case 5: //
				life.Forage(ratio);
				break;
			case 6: //
				life.Forage(ratio);
				break;
			case 7: //
				break;
			case 8: //
				break;
			default:
				System.out.println("gene error");
				
		}
	}
	
	public void GetStats(Life life)
	{
		for (int x = 0; x < life.bodyGenes.length() / 2; x++)
		{
			switch (GenesAt(life.bodyGenes.substring(x, x+2)))
			{
				case 0: //str
					life.str ++;
					break;
				case 1: //def
					life.def++;
					break;
				case 2: //spd
					life.spd++;
					life.mp = ++life.maxmp;
					break;
				case 3: //int
					life.mnd++;
					life.ip = ++life.maxip;
					break;
				case 4: //offspring size
					life.offspring_size++;
					break;
				case 5: //body size
					life.hp = life.maxhp += 10;
					break;
				case 6: //stomach size
					life.fp = life.maxfp += 20;
					break;
				case 7: //longevity
					life.longevity += 10;
					break;
				case 8: //stealth
					life.sp++;
					break;
				default:
					System.out.println("gene error");
					
			}
		}
		life.GetUpkeep();
	}
	
	public int GenesAt(String gene)
	{
		for (int x = 0; x < 9; x++){
			if (gene.matches(bodyGenes[x])) return x;
		}
		return -1;
	}
	public int BehaviorAt(String gene)
	{
		for (int x = 0; x < 9; x++){
			if (gene.matches(behaviorGenes[x])) return x;
		}
		return -1;
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	public void Reorder()
	{
		Random rand = new Random();
		bodyGenes = new String[9];
		behaviorGenes = new String[9];
		ArrayList<String> arr = GetGenePool();
		int counter = 0;
		while (!arr.isEmpty())
		{
			bodyGenes[counter] = arr.remove(rand.nextInt(arr.size()));
			counter++;
		}
		arr = GetGenePool();
		counter = 0;
		while (!arr.isEmpty())
		{
			behaviorGenes[counter] = arr.remove(rand.nextInt(arr.size()));
			counter++;
		}
		/*
		for(int x = 0; x < 9; x++)
		{
			System.out.println(bodyGenes[x]);
		}
		System.out.println("XXXX");
		for(int x = 0; x < 9; x++)
		{
			System.out.println(behaviorGenes[x]);
		}
		*/
		
	}
	private ArrayList<String> GetGenePool()
	{
		ArrayList<String> arr = new ArrayList<String>();
		arr.add("AA");
		arr.add("AB");
		arr.add("AC");
		arr.add("BA");
		arr.add("BB");
		arr.add("BC");
		arr.add("CA");
		arr.add("CB");
		arr.add("CC");
		return arr;
	}
}
