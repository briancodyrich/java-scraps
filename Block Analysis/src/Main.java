import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Scanner;
import java.util.Set;


public class Main {
	public static ArrayList<Letter> letters;

	public static void main(String[] args) throws IOException {
		BufferedReader reader = new BufferedReader(new FileReader("blocks.txt"));
		BufferedReader dictionary = new BufferedReader(new FileReader("dictionary.txt"));
		letters = new ArrayList<Letter>();
		String line = null;
		
		for (int x = 0; x < 26; x++)
		{
			letters.add(new Letter());
		}
		int num = 1;
		while ((line = reader.readLine()) != null) 
		{
			Scanner scan = new Scanner(line);
			//System.out.println(line);
			while (scan.hasNext())
			{
				String str = scan.next();
				letters.get(str.codePointAt(0) - 65).addId(num);
			}
		//	System.out.println("");
			num++;
			scan.close();
		}
		reader.close();
		
		while ((line = dictionary.readLine()) != null) 
		{
			Scanner scan = new Scanner(line);
		//	System.out.println(line);
			if ( line.length() > 16) break;
			WordExists(line);
		}
		reader.close();
	}
	
	public static boolean WordExists(String word) throws IOException
	{
		ArrayList<ArrayList<Integer>> possibilities = new ArrayList<ArrayList<Integer>>();
		for(int x = 0; x < word.length(); x++)
		{
			if (Character.toUpperCase(word.codePointAt(x)) < 65 || Character.toUpperCase(word.codePointAt(x)) > 90) return false;
			if (letters.get(Character.toUpperCase(word.codePointAt(x)) - 65).list.size() == 0)
			{
			//	System.out.println("missing letter");
				return false;
			}
			possibilities.add(letters.get(Character.toUpperCase(word.codePointAt(x)) - 65).list);
			//System.out.print(Character.toUpperCase(word.charAt(x)) + " ");
		}
		//System.out.println("");
		ArrayList<Integer> max = new ArrayList<Integer>();
		ArrayList<Integer> counter = new ArrayList<Integer>();
		for (int x = 0; x < possibilities.size(); x++) //first letter
		{
			max.add(possibilities.get(x).size());
			counter.add(0);
			//System.out.println("size: " + possibilities.get(x).size());
			for (int y = 0; y < possibilities.get(x).size(); y++)
			{
				//System.out.println(possibilities.get(x).get(y));
			}
			//System.out.println("next");
		}
	//	System.out.println(counter);
		///System.out.println(max);
		//System.out.println(counter.size());
		//System.out.println("start");
		while(counter.get(0) < max.get(0))
		{
			ArrayList<Integer> current = new ArrayList<Integer>();
			for(int x = 0; x < counter.size(); x++)
			{
				//System.out.println("cx: " + x);
				current.add(possibilities.get(x).get(counter.get(x)));
			}
			counter.set(counter.size() - 1, counter.get(counter.size() - 1) + 1);
			for(int x = counter.size() -1; x > 0; x--)
			{
				//System.out.println(x);
				if (counter.get(x) == max.get(x))
				{
					counter.set(x-1, counter.get(x-1) + 1);
					counter.set(x, 0);
				}
			}
			//System.out.println(current);
			Set<Integer> set = new HashSet<Integer>(current);
			if (set.size() == current.size())
			{
				PrintWriter writer = new PrintWriter(new FileWriter("out.txt", true));
				writer.println(word + " " + current);
				writer.close();
				return true;
			}
		}
		return false;
	}

}
